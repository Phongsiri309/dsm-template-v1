import styled from "vue-styled-components";

const theme = {
  color: {
    primary: "#00bcd4",
    secondary: "#ff9800",
    success: "#4caf50",
    warning: "#ffeb3b",
    danger: "#f44336",
  },
  background: "",
};

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  flex-wrap: wrap;
`;

export const Header = styled("header", theme)`
  display: flex;
  flex-direction: row;
  position: sticky;
  top: 0;
  z-index: 1;
  width: 100%;
  height: 75px;
  flex-wrap: wrap;
  align-items: center;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  background-color: ${(props) =>
    props.background ? props.background : theme.color.primary};
  @media screen and (max-width: 414px) {
    .nav {
      justify-content: center;
    }
  }
`;

export const Sider = styled("div", theme)`
  display: flex;
  flex-direction: column;
  width: 300px;
  min-height: 100vh;
  flex-wrap: wrap;
  align-items: center;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  background-color: ${(props) =>
    props.background ? props.background : theme.color.primary};
  @media screen and (max-width: 375px) {
    min-height: 100%;
  }
  @media screen and (max-width: 768px) {
    min-height: 100%;
    width: 100%;
  }
`;

export const Content = styled.div`
  align-self: center;
  background-color: #fff;
  width: 80vw;
  /* min-height: calc(100vh - 12.5rem); */
  min-height: 100vh;
  margin: 10px 0;
  /* padding: 10px; */
  position: relative;
  @media screen and (max-width: 1600px) {
    width: 100vw;
    padding: 10px;
  }
`;

export const Footer = styled("footer", theme)`
  width: 100%;
  min-height: 105px;
  display: flex;
  color: #fff;
  padding: 20px;
  /* align-items: center;
  justify-content: center; */
  background-color: ${(props) =>
    props.background ? props.background : theme.color.primary};
`;

export const NoNav = styled("header", theme)`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 75px;
`;

export const HomeContent = styled("div", theme)`
  display: flex;
  width: 100%;
  min-height: calc(100vh - 11.25rem);
  margin: 0 0 10px 0;
  justify-content: center;
  align-items: center;
`;

export const RowWrapper = styled("div", theme)`
  display: flex;
  flex-direction: row;
  gap: 10px;
  width: 100%;
  @media screen and (max-width: 375px) {
    flex-direction: column;
    align-items: center;
    padding: 5px;
  }
  @media screen and (max-width: 768px) {
    flex-direction: column;
    align-items: center;
  }
`;

export const ColWrapper = styled("div", theme)`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  gap: 10px;
`;

export const SectionConent = styled("section", theme)`
  min-height: 100vh;
  width: 100%;
`;
