import styled from "vue-styled-components";
import SearchBox from "./components/SearchBox.vue";
import GridMenu from "./components/GridMenu.vue";
import HomeSearch from "./components/HomeSearch.vue";
import Card from "./components/Card.vue";
import List from "./components/List.vue";
import Tab from "./components/Tab.vue";
import Burger from "./components/Burger.vue";

const Variable = {
  color: "",
  background: "",
  maxWidth: "",
  height: 11,
  width: 11,
  center: "center",
};

export const Logo = styled("div", Variable)`
  margin: 10px;
  width: 200px;
  display: flex;
  font-size: 2em;
  font-weight: bold;
  color: ${(props) => (props.color ? props.color : null)};
  justify-content: center;
  align-items: center;
  background-color: ${(props) => (props.background ? props.background : null)};
`;

export const Search = styled(SearchBox)`
  max-width: 250px !important;
  height: 30px;
`;

export const AppMenu = styled(GridMenu)``;

export const InputSearch = styled(HomeSearch)`
  max-width: 28.125rem !important;
`;

export const CardPreview = styled(Card, Variable)`
  width: ${(props) => (props.width ? props.width : Variable.width)}rem;
  height: ${(props) => (props.height ? props.height : Variable.height)}rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  margin: 0 5px;
  .divider {
    width: 100%;
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);
    margin-top: 5px;
  }

  .card-header {
    font-size: 1.5em;
    font-weight: bold;
  }

  .card-body {
    font-size: 1em;
    display: flex;
    height: 100%;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
  }
`;

export const DataList = styled(List, Variable)`
  .row {
    margin: unset !important;
  }
`;

export const Navigator = styled(Tab, Variable)``;

export const BurgerMenu = styled(Burger, Variable)`
  position: absolute;
  right: 15px;
  top: 10px;
  display: none;
  @media screen and (max-width: 768px) {
    display: block;
  }
`;
